# README #

To install and setup

### What is this repository for? ###

* Uses Intervention Image library in an Artisan Command

### How do I get set up? ###

* include the intervention image library, more info here http://image.intervention.io/getting_started/installation
* Install GD or Imagemagick stated here http://image.intervention.io/getting_started/installation
* Open your application’s config/app.php file and add it to your providers and aliases. This is where they both get really handy.

'providers' => [
	...
	Intervention\Image\InterventionImageServiceProvider::class,
    Intervention\Image\ImageServiceProvider::class,
],
 
'aliases' => [
	...
	'IImg' => Intervention\Image\InterventionImageFacade::class,
	'Image' => Intervention\Image\Facades\Image::class,
],


### How to use ###

* Syntax

php artisan iimage:resize {file/folder path} {width | int | auto | null} {height | int | auto | null}

{file/folder path}
- it can resize an image file or a folder and recursively resize it if its folder

{width | int | auto | null}
- resize the image according to width: 
-- int: integer put any number to resize
sample: php artisan iimage:resize /path/folder 300 500
- this will resize image to width 300 height 500

-- auto: auto width or height, constrain aspect ratio
sample: (width) php artisan iimage:resize /path/folder auto 500
- resize the image to a height of 500 and constrain aspect ratio (auto width)

sample: (height) php artisan iimage:resize /path/folder 500 auto
- resize the image to a width of 500 and constrain aspect ratio (auto height)

-- null: if width or height is set to null meaning it will resize only either width or height
sample: (width) php artisan iimage:resize /path/folder 500 null
resize only the width of the image

sample: (height) php artisan iimage:resize /path/folder null 500
resize only the height of the image
