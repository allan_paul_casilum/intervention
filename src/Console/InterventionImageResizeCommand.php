<?php

namespace Intervention\Image\Console;

use Illuminate\Console\Command;
use File;
use Storage;
use Image;
use Intervention\Image\InterventionImage;
class InterventionImageResizeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'iimage:resize {file} {width} {height}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Intervention Image Resize';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $file = $this->argument('file');
        
        $width = $this->argument('width');
        if( !$width ){
			$width = '';
		}elseif( $width == 'null' ){
			$width = null;
		}

        $height = $this->argument('height');
        if( !$height ){
			$height = '';
		}elseif( $height == 'null' ){
			$height = null;
		}
		
        $this->line('Intervention Image, File/Folder: ' . $file . ' width : '.$width . ' height : '. $height);
		
		$allowedExt = array(
			'jpg',
			'JPG',
			'jpeg',
			'JPEG',
			'png',
			'PNG',
			'bmp',
			'BMP',
		);
		
		$iimage = new InterventionImage();
		
        if( File::exists($file) ){
			$this->line($file . ' exists');
			if( is_dir($file) ){
				$this->line('path is directory');
				$files = File::allFiles($file);
				
				foreach($files as $k => $v){
					$ext = File::extension($v);
					if(in_array($ext, $allowedExt) ){
						$file_parts = pathinfo($v);
						//$new_filename = $file_parts['dirname'] .'/'. $file_parts['filename'] . '-' . $width . 'x' . $height . '.' . $file_parts['extension']; 
						$this->line('image name : ' . $v);
						$iimage->resize($v, $width, $height);
					}
				}
			}else{
				$this->line('path is file');
				$this->line('image name : ' . $file);
				$iimage->resize($file, $width, $height);
			}
		}else{
			$this->line($file . ' doesn\'t exists');
		}

    }
}
