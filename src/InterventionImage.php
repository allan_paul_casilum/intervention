<?php
namespace Intervention\Image;
use File;
use Storage;
use Image;
class InterventionImage
{
	
	public function resize($file, $width, $height){
		$img = Image::make($file);
		if( $height == 'auto' ){
			$img->resize($width, null, function ($constraint) {
				$constraint->aspectRatio();
			})->save($file);
		}
		if( $width == 'auto' ){
			$img->resize(null, $height, function ($constraint) {
				$constraint->aspectRatio();
			})->save($file);
		}
		if( is_null($height) ){
			$img->resize($width, null)->save($file);
		}
		if( is_null($width) ){
			$img->resize(null, $height)->save($file);
		}
		if( $width != 'auto' 
			&& $height != 'auto' 
			&& !is_null($width)
			&& !is_null($height)
		){
			$img->resize($width, $height)->save($file);
		}
	}
	
	public function __construct(){}
}
