<?php
namespace Intervention\Image;
use Illuminate\Support\Facades\Facade;

class InterventionImageFacade extends Facade
{
    protected static function getFacadeAccessor() { 
        return 'intervention-image';
    }
}
