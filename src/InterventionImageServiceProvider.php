<?php

namespace Intervention\Image;

use Illuminate\Support\ServiceProvider;

class InterventionImageServiceProvider extends ServiceProvider
{
	protected $commands = [
        'Intervention\Image\Console\InterventionImageResizeCommand',
    ];
    
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('intervention-image', function() {
            return new InterventionImage;
        });
        $this->commands($this->commands);
    }
}
